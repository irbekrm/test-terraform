Creates a single instance ElasticBeanstalk environment. (An Autoscaling group with a single `t3.micro` EC2 instance).

ElasticBeanstalk automatically creates a security group that whitelists port 80 for all IPs. Additionally SSH access is provided via SSH (port 22)
for `2.81.203.136`(Cs office) and `62.254.158.101` (TUI London Bridge).

This module calls terraform module from `tda.devops.terraform/elasticbeanstalk/elasticbeanstalk` which in turn uses terraform remote state from 
`tda.devops.terraform/elasticbeanstalk/iam` and `tda.devops.terraform/elasticbeanstalk/infra` so these two modules need have been called.

It uses `tda.devops.terraform/elasticbeanstalk/elasticbeanstalk` as a terraform child module accessed from a private Bitbucket repo
so the caller needs to have permissions to access it.

It needs to have an [ElasticBeanstalk Application Source Bundle](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/applications-sourcebundle.html) containing
a [`Dockerrun.aws.json`](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/single-container-docker-configuration.html) file at `"../bundle.zip"`. To change the
location or name of the zip file, modify this line `local_app_source_bundle_path = "../bundle.zip"` in `variables.tf`