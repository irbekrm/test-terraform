variable "region" {
    type = "string"
    default = "eu-central-1"
}

variable "project" {
    type = "string"
    default = "tda.devops.terraform.bookings"
}