provider "aws" {
    region = "${var.region}"
}

terraform {
    backend "s3" {
        bucket = "eb-test9106"
        key = "tfstates/tda.bookings/terraform.tfstate"
        region = "eu-central-1"
    }
}

data "terraform_remote_state" "infra" {
  backend = "s3"
  config {
    bucket = "eb-test9106"
    key = "tfstates/infra/terraform.tfstate"
    region = "eu-central-1"
  }
}

module "main" {
    source = "../tda.devops.terraform/elasticbeanstalk/elasticbeanstalk"
    service = "tda-bookings"
    bucket_name = "eb-test9106"
    ssh_source_restriction = "62.254.158.10/32"
    local_app_source_bundle_path = "../bundle.zip"
    additional_security_group = "${aws_security_group.additional.id}"
    zone_id = "Z1BU77W1IKDK95"
}

resource "aws_security_group" "additional" {
    name = "tda.devops.terraform.bookings"
    description = "Allow inbound traffic from provided CIDR blocks"
    vpc_id = "${data.terraform_remote_state.infra.vpc_id}"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["2.81.203.136/32"]
        description = "SSH access for COCAS"
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        description = "Temporary access"
    }
}
